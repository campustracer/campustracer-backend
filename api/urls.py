from api import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r"User", views.UserView, basename="user")
router.register(r"UserMasterKey", views.UserMasterKeyView, basename="master-key")
router.register(r"CheckIn", views.CheckInView, basename="check-in")
router.register(
    r"UserAttendedLecture",
    views.UserAttendedLectureView,
    basename="user-attended-lecture",
)
router.register(
    r"LectureAttendedByUser",
    views.LectureAttendedByUserView,
    basename="lecture-attended-by-user",
)
router.register(r"InfectionCase", views.InfectionCase, basename="infection-case")
urlpatterns = router.urls
