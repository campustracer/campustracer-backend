from rest_framework.exceptions import APIException


class BadHttpParameter(APIException):
    status_code = 400
    default_detail = 'Could not parse query parameter.'
    default_code = 'bad_request'
