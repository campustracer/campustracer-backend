import logging
from datetime import date, datetime, timedelta

from django.contrib.auth.models import User as DjangoUser
from django.contrib.auth.hashers import make_password
from django.core.management.base import BaseCommand
from django.db import transaction

from api import crypto, models

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Initialize database with test data"

    _password = "C/yVUTv5o38H1B8yw3LB"
    _hash = make_password(_password)

    @transaction.atomic
    def handle(self, *args, **options):
        # use ./manager.py flush to clear db
        self.create_superuser()
        self.create_lecturers(10)
        self.create_students(100)

    def create_superuser(self) -> None:
        root = DjangoUser.objects.create(
            username="root",
            is_staff=True,
            is_superuser=True,
            password=self._hash,
        )
        models.User.objects.create(
            user=root,
            role=models.Role.UNIVERSITY,
            encContactData="",
        )
        LOGGER.info(f"Superuser root with password {self._password} created")

    def create_lecturers(self, amount: int) -> None:
        r = range(10, 10 + amount)

        DjangoUser.objects.bulk_create(
            [DjangoUser(id=n, username=f"Lecturer{n}", password=self._hash) for n in r]
        )

        models.User.objects.bulk_create(
            [
                models.User(user_id=n, role=models.Role.LECTURER, encContactData="")
                for n in r
            ]
        )

        now = datetime.now()
        models.UserMasterKey.objects.create(
            user_id=10,
            createdAt=now,
            publicKey="Lecturer1PublicKey",
            encPrivateKey="encPrivateKey",
            signature=crypto.get_mock_signature("Lecturer1PublicKey", [10, now]),
        )

        LOGGER.info(f"{amount} lecturers created")

    def create_students(self, amount: int) -> None:
        r = range(10000, 10000 + amount)

        DjangoUser.objects.bulk_create(
            [DjangoUser(id=n, username=f"Student{n}", password=self._hash) for n in r]
        )

        models.User.objects.bulk_create(
            [
                models.User(user_id=n, role=models.Role.STUDENT, encContactData="")
                for n in r
            ]
        )

        now = datetime.now()
        master_pub = "Student 1 Master Public Key"
        master_key = models.UserMasterKey.objects.create(
            user_id=10000,
            createdAt=now,
            publicKey=master_pub,
            encPrivateKey="Enc(Student 1 Master Private Key)",
            signature=crypto.get_mock_signature(master_pub, [10000, now]),
        )
        pre_generated_encryption_keys = []
        valid_to = date.today()
        for i in range(1, 7):
            weekly_pub = "Student 1 PGEK " + str(i)
            valid_from = valid_to
            valid_to = valid_to + timedelta(days=7)
            pre_generated_encryption_keys.append(
                models.PreGeneratedEncryptionKey(
                    masterKey=master_key,
                    publicKey=weekly_pub,
                    validFrom=valid_from,
                    validTo=valid_to,
                    signature=crypto.get_mock_signature(
                        master_pub, [weekly_pub, i, now]
                    ),
                )
            )
        models.PreGeneratedEncryptionKey.objects.bulk_create(
            pre_generated_encryption_keys
        )

        LOGGER.info(f"{amount} students created")
