from __future__ import annotations
from typing import Optional
from datetime import date, datetime

from django.db import models
from django.contrib.auth.models import User as DjangoUser

from api import crypto


class Role(models.TextChoices):
    STUDENT = "STUDENT"
    LECTURER = "LECTURER"
    UNIVERSITY = "UNIVERSITY"


class User(models.Model):
    user = models.OneToOneField(
        DjangoUser,
        on_delete=models.CASCADE,
        db_column="id",
        unique=True,
        primary_key=True,
    )
    role = models.CharField(max_length=10, choices=Role.choices)
    encContactData = models.TextField()
    changedAt = models.DateTimeField(default=datetime.now)

    def __str__(self) -> str:
        return self.username

    @property
    def username(self) -> str:
        return self.user.get_username()

    @property
    def isStudent(self) -> bool:
        return self.role == Role.STUDENT

    @property
    def isLecturer(self) -> bool:
        return self.role == Role.LECTURER

    @property
    def isUniversity(self) -> bool:
        return self.role == Role.UNIVERSITY

    @staticmethod
    def create(username: str, **kwargs) -> User:
        django_user = DjangoUser.objects.create(username=username)
        return User.objects.create(user=django_user, **kwargs)

    def get_master_key(self) -> Optional[UserMasterKey]:
        return UserMasterKey.objects.filter(user=self, invalidatedAt=None).first()


class UserMasterKey(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="masterKeys", db_column="userId"
    )
    publicKey = models.TextField()
    encPrivateKey = models.TextField()
    createdAt = models.DateTimeField(default=datetime.now)
    invalidatedAt = models.DateTimeField(default=None, null=True, blank=True)
    signature = models.TextField()

    def __str__(self) -> str:
        return (
            f"Master key for {self.user.__str__()}"
            f" ({'invalid' if self.invalidatedAt else 'valid'})"
        )

    def signature_valid(self) -> bool:
        return crypto.validate_signature(
            self.publicKey,
            self.signature,
            [self.user.pk, self.createdAt],
        )


class PreGeneratedEncryptionKey(models.Model):
    masterKey = models.ForeignKey(
        UserMasterKey,
        on_delete=models.CASCADE,
        related_name="preGeneratedEncryptionKeys",
        db_column="masterKeyId",
    )
    publicKey = models.TextField()
    signature = models.TextField()
    validFrom = models.DateTimeField()
    validTo = models.DateTimeField()

    def __str__(self) -> str:
        return (
            f"Weekly key for {self.masterKey.user.__str__()}"
            f" from {self.validFrom} - {self.validTo}"
            f" (master: {'invalid' if self.masterKey.invalidatedAt else 'valid'},"
            f" time: {'valid' if self.time_valid() else 'invalid'})"
        )

    def time_valid(self) -> bool:
        now = datetime.now()
        return self.validFrom < now and self.validTo >= now

    def signature_valid(self) -> bool:
        return crypto.validate_signature(
            self.masterKey.publicKey,
            self.signature,
            [self.publicKey, self.validFrom, self.validTo],
        )


class UserAttendedLecture(models.Model):
    check_in_private_key_hash = models.CharField(max_length=crypto.HASH_CHAR_LENGTH)
    createdAt = models.DateField(default=date.today)
    encData = models.TextField()


class LectureAttendedByUser(models.Model):
    lecture_private_key_hash = models.CharField(max_length=crypto.HASH_CHAR_LENGTH)
    createdAt = models.DateField(default=date.today)
    encData = models.TextField()


class InfectionCase(models.Model):
    createdAt = models.DateField(default=date.today)
    encData = models.TextField()
    encMedicalProof = models.TextField()
