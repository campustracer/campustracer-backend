from typing import Any

from datetime import datetime, timedelta

from django.db import transaction

from rest_framework import serializers
from rest_framework.generics import get_object_or_404

from api import models


class UserSerializer(serializers.Serializer):
    id = serializers.IntegerField(source="user_id", read_only=True)
    username = serializers.CharField(read_only=True)
    role = serializers.ChoiceField(choices=models.Role, read_only=True)
    encContactData = serializers.CharField()
    changedAt = serializers.DateTimeField(read_only=True)

    def update(self, instance: models.User, validated_data: Any) -> models.User:
        instance.encContactData = validated_data.get("encContactData")
        instance.changedAt = datetime.now()
        instance.save()
        return instance


class PreGeneratedEncryptionKeySerializer(serializers.Serializer):
    publicKey = serializers.CharField(
        label="User pre generated public key",
        help_text="PGEK<sup>P</sup>",
    )
    validFrom = serializers.DateTimeField(
        label="Valid from", help_text="Datetime the key becomes valid"
    )
    validTo = serializers.DateTimeField(
        label="Valid to", help_text="Last datetime the key is valid"
    )
    signature = serializers.CharField(
        label="User Weekly Key Signature",
        help_text="Sig(MK<sup>S</sup>, [PGEK<sup>P</sup>, validFrom, validTo])",
    )


class UserMasterKeySerializer(serializers.Serializer):
    id = serializers.IntegerField(
        read_only=True, label="Key id", help_text="Primary key of the UserMasterKey"
    )
    userId = serializers.IntegerField(
        source="user_id",
        read_only=True,
        label="User ID",
        help_text="Related UserID",
    )

    createdAt = serializers.DateTimeField(
        label="Created At", help_text="Datetime of Record Creation"
    )
    publicKey = serializers.CharField(
        label="User Master Key Public",
        help_text="MK<sup>P<sup>",
    )
    encPrivateKey = serializers.CharField(
        label="User Master Key Private (Encrypted)",
        help_text="Enc(TPK<sup>P</sup>, MK<sup>S</sup>)",
    )
    signature = serializers.CharField(
        label="User Master Key Signature",
        help_text="Sig(MK<sup>S</sup>, [UserID, CreatedAt])",
    )

    # will be set automatically
    invalidatedAt = serializers.DateTimeField(
        read_only=True,
        label="Invalidated At",
        help_text="Datetime of Record Invalidation",
    )

    preGeneratedEncryptionKeys = PreGeneratedEncryptionKeySerializer(
        many=True,
        required=False,
        allow_null=True,
        help_text="Public keys derived from this master key",
    )

    @transaction.atomic
    def create(self, validated_data: Any) -> models.UserMasterKey:
        user = get_object_or_404(models.User, user=self.context["request"].user)
        old_keys = models.UserMasterKey.objects.filter(user=user, invalidatedAt=None)

        createdAt = validated_data.get("createdAt")
        now = datetime.now()
        if createdAt < now - timedelta(hours=1):
            raise serializers.ValidationError(
                {"createdAt": "Key cannot be older than an hour"}
            )
        if createdAt > now + timedelta(minutes=5):
            raise serializers.ValidationError(
                {"createdAt": "Key cannot be from the future"}
            )
        if old_keys.exists() and createdAt <= old_keys.latest("createdAt").createdAt:
            raise serializers.ValidationError(
                {"createdAt": "Key cannot be older than existing keys"}
            )

        master = models.UserMasterKey.objects.create(
            user=user,
            createdAt=createdAt,
            publicKey=validated_data.get("publicKey"),
            encPrivateKey=validated_data.get("encPrivateKey"),
            signature=validated_data.get("signature"),
        )
        if not master.signature_valid():
            raise serializers.ValidationError(
                {"signature": "Master signature not valid"}
            )
        old_keys = old_keys.exclude(id=master.pk)

        # add preGeneratedEncryptionKeys for students only
        if user.isStudent:
            if not validated_data.get("preGeneratedEncryptionKeys"):
                raise serializers.ValidationError(
                    {"preGeneratedEncryptionKeys": "Field is required"}
                )
            pre_generated_encryption_keys = []
            for key in validated_data.get("preGeneratedEncryptionKeys"):
                key_obj = models.PreGeneratedEncryptionKey(
                    masterKey=master,
                    publicKey=key.get("publicKey"),
                    validFrom=key.get("validFrom"),
                    validTo=key.get("validTo"),
                    signature=key.get("signature"),
                )
                if not key_obj.signature_valid():
                    raise serializers.ValidationError(
                        {"signature": "Signature not valid"}
                    )
                pre_generated_encryption_keys.append(key_obj)
            models.PreGeneratedEncryptionKey.objects.bulk_create(
                pre_generated_encryption_keys
            )
        else:
            if validated_data.get("preGeneratedEncryptionKeys", False):
                raise serializers.ValidationError(
                    {"preGeneratedEncryptionKeys": "Only students can have these"}
                )

        # invalidate all old keys
        for key in old_keys:
            key.invalidatedAt = createdAt
        models.UserMasterKey.objects.bulk_update(old_keys, fields=["invalidatedAt"])

        return master


class CheckInSerializer(serializers.Serializer):
    createdAt = serializers.DateField(
        write_only=True, label="Created At", help_text="Date of Record Creation"
    )
    checkInPrivateKeyHash = serializers.CharField(
        write_only=True, label="Check-in private key hash", help_text=""
    )
    encUserAttendedLecture = serializers.CharField(
        write_only=True,
        label="User Attended Lecture (Encrypted)",
        help_text="Enc(EP<sup>P</sup>, ["
        "Enc(UK<sup>P</sup>, LK<sup>S</sup>),"
        "signedById"
        "Sig(SP<sup>S</sup>, [Enc(UK<sup>P</sup>, LK<sup>S</sup>), userId, date])"
        "])",
    )
    lecturePrivateKeyHash = serializers.CharField(
        write_only=True,
        label="Lecture Attended By User Id",
        help_text="Hash([N=Rand(1, 100), Hash(LK<sup>S</sup>)])",
    )
    encLectureAttendedByUser = serializers.CharField(
        write_only=True,
        label="Lecture Attended by User (Encrypted)",
        help_text="Enc(LK<sup>P</sup>, ["
        "userId, signedById, Sig(SP<sup>S</sup>, [userId, date])"
        "])",
    )

    @transaction.atomic
    def create(self, validated_data: Any) -> models.UserAttendedLecture:
        instance = models.UserAttendedLecture.objects.create(
            check_in_private_key_hash=validated_data.get("checkInPrivateKeyHash"),
            encData=validated_data.get("encUserAttendedLecture"),
        )
        models.LectureAttendedByUser.objects.create(
            lecture_private_key_hash=validated_data.get("lecturePrivateKeyHash"),
            encData=validated_data.get("encLectureAttendedByUser"),
        )

        if validated_data.get("createdAt") != instance.createdAt:
            raise serializers.ValidationError(
                {"createdAt": "createdAt is not today. Is your local date wrong?"}
            )

        return instance


class UserAttendedLectureSerializer(serializers.Serializer):
    checkInPrivateKeyHash = serializers.CharField(
        source="check_in_private_key_hash",
        read_only=True,
        label="User Attended Lecture Id",
        help_text="",
    )
    createdAt = serializers.DateField(
        read_only=True, label="Created At", help_text="Date of Record Creation"
    )
    encData = serializers.CharField(
        read_only=True,
        label="User Attended Lecture (Encrypted)",
        help_text="Enc(EP<sup>P</sup>, ["
        "Enc(UK<sup>P</sup>, LK<sup>S</sup>),"
        "signedById"
        "Sig(SP<sup>S</sup>, [Enc(UK<sup>P</sup>, LK<sup>S</sup>), userId, date])"
        "])",
    )


class LectureAttendedByUserSerializer(serializers.Serializer):
    lecturePrivateKeyHash = serializers.CharField(
        source="lecture_private_key_hash",
        read_only=True,
        label="Lecture Attended By User Id",
        help_text="Lecture Attended By User Id",
    )
    createdAt = serializers.DateField(
        read_only=True, label="Created At", help_text="Date of Record Creation"
    )
    encData = serializers.CharField(
        read_only=True,
        label="Lecture Attended by User (Encrypted)",
        help_text="Enc(LK<sup>P</sup>, ["
        "userId, signedById, Sig(SP<sup>S</sup>, [userId, date])"
        "])",
    )


class InfectionCaseSerializer(serializers.Serializer):
    id = serializers.IntegerField(
        read_only=True, label="Record Id", help_text="Surrogate key for the record"
    )
    createdAt = serializers.DateField(
        read_only=True, label="Created At", help_text="Date of Record Creation"
    )
    encData = serializers.CharField(
        label="Infaction Case Data (Encrypted)",
        help_text="Enc(UK<sup>P</sup>, ["
        "**userId**, "
        "**PGEKs<sup>S</sup>**: Generated weekly keys for the timeframe, "
        "**SDKs<sup>S</sup>**: Generated daily keys for the timeframe, "
        "**infectionStart**: Date to start infection tracing, "
        "**infectionEnd**: Date to stop infection tracing, "
        "]",
    )
    encMedicalProof = serializers.CharField(
        label="Infaction Case Medical Proof (Encrypted)",
        help_text="Enc(UK<sup>P</sup>, ["
        "**medicalProof**: Uploaded medical proof, i.e. doctors note"
        "]",
    )

    def __init__(self, *args, **kwargs) -> None:
        """Remove encMedicalProof for list view"""
        super().__init__(*args, **kwargs)
        if self.context["view"].action == "list":
            self.fields.pop("encMedicalProof")

    def create(self, validated_data: Any) -> models.InfectionCase:
        instance = models.InfectionCase.objects.create(
            encData=validated_data.get("encData"),
            encMedicalProof=validated_data.get("encMedicalProof")
        )
        return instance
