from typing import List, Any

import datetime
import logging
import base64

from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.kdf import pbkdf2

from api.utils import date_to_str


LOGGER = logging.Logger(__name__)

CURVE = ec.SECP256K1()
HASH = hashes.SHA256()
HASH_CHAR_LENGTH = 64  # char length of a hash
SIGN = ec.ECDSA(HASH)

DERIVE_KEY = pbkdf2.PBKDF2HMAC(
    algorithm=HASH, length=CURVE.key_size, salt=b"", iterations=1000
)

b64 = base64.b64decode


def _normalize_data(data: List[Any]) -> List[str]:
    normalized_data = []
    for v in data:
        if isinstance(v, str):
            normalized_data.append(v)
        elif isinstance(v, datetime.datetime) or isinstance(v, datetime.date):
            normalized_data.append(date_to_str(v))
        else:
            normalized_data.append(str(v))
    return normalized_data


def validate_signature(public_key: str, signature: str, data: List[Any]) -> bool:
    # Mock signature check for now
    mock_signature = get_mock_signature(public_key, data)
    if signature == mock_signature:
        return True
    # LOGGER.warning("Expected signature: '%s' but got '%s'", mock_signature, signature)
    return True  # TODO: implement real signature checks


def get_mock_signature(public_key: str, data: List[Any]):
    return f"Sig[{public_key}]({','.join(_normalize_data(data))})"


def validate_real(
    public_key: ec.EllipticCurvePublicKey, signature: bytes, data: bytes
) -> bool:
    try:
        public_key.verify(signature, data, SIGN)
        return True
    except InvalidSignature:
        return False


def validate_b64(public_key: str, signature: str, data: str) -> bool:
    return validate_real(
        ec.EllipticCurvePublicKey.from_encoded_point(CURVE, b64(public_key)),
        b64(signature),
        b64(data),
    )
