from typing import Optional

from rest_framework import permissions, request, views

from api import models


class AuthMixin:
    _user: Optional[models.User] = None

    def get_user(self, request) -> models.User:
        if not self._user:
            self._user = models.User.objects.get(user=request.user)
        return self._user

    def is_authenticated(self, request):
        return bool(
            request.user and request.user.is_authenticated and self.get_user(request)
        )

    def is_student(self, request):
        return self.is_authenticated(request) and self.get_user(request).isStudent

    def is_lecturer(self, request):
        return self.is_authenticated(request) and self.get_user(request).isLecturer

    def is_university(self, request):
        return self.is_authenticated(request) and self.get_user(request).isUniversity


class IsAuthenticated(AuthMixin, permissions.BasePermission):
    """Allows access only to unauthenticated users"""

    def has_permission(self, request: request.Request, view: views.APIView) -> bool:
        return self.is_authenticated(request)


class IsNotAuthenticatedWriteOnly(AuthMixin, permissions.BasePermission):
    """Allows write access only to unauthenticated users"""

    def has_permission(self, request: request.Request, view: views.APIView) -> bool:
        return not (
            self.is_authenticated(request) or request.method in permissions.SAFE_METHODS
        )


class IsAuthenticatedWriteOnly(AuthMixin, permissions.BasePermission):
    """Allows write access to all authenticated users"""

    def has_permission(self, request: request.Request, view: views.APIView) -> bool:
        return (
            self.is_authenticated(request)
            and request.method not in permissions.SAFE_METHODS
        )


class IsStudent(AuthMixin, permissions.BasePermission):
    """Allows access only to authenticated students"""

    def has_permission(self, request: request.Request, view: views.APIView) -> bool:
        return self.is_student(request)


class IsLecturer(AuthMixin, permissions.BasePermission):
    """Allows access only to authenticated lecturers"""

    def has_permission(self, request: request.Request, view: views.APIView) -> bool:
        return self.is_lecturer(request)


class isUniversity(AuthMixin, permissions.BasePermission):
    """Allows access only to authenticated lecturers"""

    def has_permission(self, request: request.Request, view: views.APIView) -> bool:
        return self.is_university(request)


class IsLecturerReadOnly(AuthMixin, permissions.BasePermission):
    """Allows read only access only to authenticated lecturers"""

    def has_permission(self, request: request.Request, view: views.APIView) -> bool:
        return request.method in permissions.SAFE_METHODS and self.is_lecturer(request)


class isUniversityReadOnly(AuthMixin, permissions.BasePermission):
    """Allows read only access only to authenticated university staff"""

    def has_permission(self, request: request.Request, view: views.APIView) -> bool:
        return request.method in permissions.SAFE_METHODS and self.is_university(
            request
        )
