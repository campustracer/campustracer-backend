from django.contrib import admin

from api import models


@admin.register(models.User)
class StudentAdmin(admin.ModelAdmin):
    pass


@admin.register(models.UserMasterKey)
class StudentMasterKeyAdmin(admin.ModelAdmin):
    pass


@admin.register(models.PreGeneratedEncryptionKey)
class StudentWeeklyKeyAdmin(admin.ModelAdmin):
    pass


@admin.register(models.UserAttendedLecture)
class StudentAttendedLectureAdmin(admin.ModelAdmin):
    pass


@admin.register(models.LectureAttendedByUser)
class LectureAttendedByStudentAdmin(admin.ModelAdmin):
    pass


@admin.register(models.InfectionCase)
class InfectionCase(admin.ModelAdmin):
    pass
