import logging

from typing import Optional, Union
from datetime import date, datetime

from api.exceptions import BadHttpParameter

LOG = logging.getLogger(__name__)


def date_to_str(time: Union[date, datetime]) -> str:
    """Prints a datetime object in iso format with milliseconds"""
    if isinstance(time, datetime):
        return time.isoformat(timespec="seconds")
    elif isinstance(time, date):
        return time.isoformat()


def str_to_datetime(date: str) -> Optional[datetime]:
    """Read an ISO date from a string"""
    if not date:
        # ignore empty strings
        return None
    try:
        return datetime.fromisoformat(date)
    except Exception:
        LOG.info('Could not read date from string: "%s"', date)
        raise BadHttpParameter


def str_to_int(value: str) -> Optional[int]:
    """Read an int from a string"""
    if not value:
        # ignore empty strings
        return None
    try:
        return int(value)
    except Exception:
        LOG.info('Could not read int from string: "%s"', value)
        raise BadHttpParameter
