from rest_framework import status
from api import models

from api.tests.utils import APIEndpointTest


def get_record(encContactData: str = "This is the encrypted contact data"):
    return {"encContactData": encContactData}


class UserTest(APIEndpointTest):
    def test_get_role(self):
        student = self.get_student()
        client = self.get_client(student)
        response = client.get(["user-detail", student.username], get_record())
        role = response.data["role"]
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(role, models.Role.STUDENT)

        lecturer = self.get_lecturer()
        client = self.get_client(lecturer)
        response = client.get(["user-detail", lecturer.username], get_record())
        role = response.data["role"]
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(role, models.Role.LECTURER)

    def test_set_enc_contact_data(self):
        student = self.get_student()
        client = self.get_client(student)
        response = client.put(["user-detail", student.username], get_record())
        self.assertEqual(response.status_code, status.HTTP_200_OK)

