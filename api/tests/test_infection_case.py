from rest_framework import status

from api.tests.utils import APIEndpointTest


def get_record(
    data: str = "This is an encrypted infection case",
    medicalProof: str = "This is the encrypted medical proof",
):
    return {"encData": data, "encMedicalProof": medicalProof}


class InfectionCaseTest(APIEndpointTest):
    def test_create_infection_case(self):
        client = self.get_client()
        response = client.post("infection-case-list", get_record())
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_infection_case_as_student(self):
        student = self.get_student()
        client = self.get_client(user=student)
        response = client.post("infection-case-list", get_record())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_infection_case_as_lecturer(self):
        lecturer = self.get_lecturer()
        client = self.get_client(user=lecturer)
        response = client.post("infection-case-list", get_record())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_infection_case_as_lecturer(self):
        client = self.get_client()
        response = client.post("infection-case-list", get_record())
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        lecturer = self.get_lecturer()
        client = self.get_client(user=lecturer)
        response = client.get("infection-case-list", get_record())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_infection_case_as_university(self):
        client = self.get_client()
        response = client.post("infection-case-list", get_record())
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        university = self.get_university()
        client = self.get_client(user=university)
        response = client.get("infection-case-list", get_record())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertTrue("encMedicalProof" not in response.data[0])

        university = self.get_university()
        client = self.get_client(user=university)
        response = client.get(
            ["infection-case-detail", response.data[0]["id"]], get_record()
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue("encMedicalProof" in response.data)
        self.assertEqual(
            response.data["encMedicalProof"], get_record()["encMedicalProof"]
        )
