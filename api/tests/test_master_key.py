from typing import Optional, Tuple
from datetime import datetime, timedelta

from rest_framework import status

from api.models import UserMasterKey
from api.tests.utils import APIEndpointTest
from api.utils import date_to_str

from api import crypto


def get_key(
    userId: int = 123,
    createdAt: datetime = datetime.now(),
    publicKey: str = "TestMasterPublicKey",
    encPrivateKey: str = "TestEncMasterPrivateKey",
    signature: str = "",
    preGeneratedEncryptionKeys: Optional[Tuple[str, ...]] = ("SWK1", "SWK2", "SWK3"),
):
    date = "2021-10-{day:02d}T01:00:00.000"
    return {
        "createdAt": date_to_str(createdAt),
        "publicKey": publicKey,
        "encPrivateKey": encPrivateKey,
        "signature": signature
        or crypto.get_mock_signature(publicKey, [userId, createdAt]),
        "preGeneratedEncryptionKeys": [
            {
                "publicKey": k,
                "validFrom": date.format(day=n * 3 + 1),
                "validTo": date.format(day=n * 3 + 4),
                "signature": crypto.get_mock_signature(
                    publicKey,
                    [k, date.format(day=n * 3 + 1), date.format(day=n * 3 + 4)],
                ),
            }
            for n, k in enumerate(preGeneratedEncryptionKeys)
        ]
        if preGeneratedEncryptionKeys
        else None,
    }


class KeyCreationTest(APIEndpointTest):
    def test_add_master_key_unauthenticated(self):
        client = self.get_client()
        response = client.post("master-key-list", get_key())
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_add_master_key_invalid_signature(self):
        client = self.get_client(user=self.get_student())

        response = client.post("master-key-list", get_key(signature="InvalidSignature"))
        # Disabled for now as signature checks are disabled
        # self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_add_master_key_as_student(self):
        student = self.get_student()
        client = self.get_client(user=student)

        response = client.post(
            "master-key-list",
            get_key(
                userId=student.pk,
                createdAt=datetime.now() - timedelta(minutes=30),
            ),
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = client.post("master-key-list", get_key(userId=student.pk))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Now there should be two keys, but only one valid
        self.assertEqual(UserMasterKey.objects.filter(user=student).count(), 2)
        self.assertEqual(
            UserMasterKey.objects.filter(user=student, invalidatedAt=None).count(), 1
        )

        other_student = self.get_student(username="other_student")
        client = self.get_client(user=other_student)
        response = client.post(
            "master-key-list",
            get_key(userId=other_student.pk),
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Check that other keys don't get invalidated
        self.assertEqual(
            UserMasterKey.objects.filter(user=student, invalidatedAt=None).count(), 1
        )
        self.assertEqual(
            UserMasterKey.objects.filter(
                user=other_student, invalidatedAt=None
            ).count(),
            1,
        )

    def test_add_master_key_as_student_without_pgeks(self):
        student = self.get_student()
        client = self.get_client(user=student)
        key = get_key(userId=student.pk)
        key.pop("preGeneratedEncryptionKeys")
        response = client.post("master-key-list", key)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_keys_with_wrong_created_at(self):
        student = self.get_student()
        client = self.get_client(user=student)

        t = datetime.now() - timedelta(days=1)
        response = client.post(
            "master-key-list", get_key(userId=student.pk, createdAt=t)
        )
        # Too old
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        t = datetime.now() - timedelta(minutes=30)
        client.post("master-key-list", get_key(userId=student.pk, createdAt=t))
        response = client.post(
            "master-key-list", get_key(userId=student.pk, createdAt=t)
        )
        # cannot create key with same createdAt time
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        t = datetime.now() + timedelta(minutes=30)
        client.post("master-key-list", get_key(userId=student.pk, createdAt=t))
        # cannot create key from the future
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_master_key_as_lecturer(self):
        lecturer = self.get_lecturer()
        client = self.get_client(user=lecturer)

        # allow PGEKs=Null
        key = get_key(
            userId=lecturer.pk,
            createdAt=datetime.now() - timedelta(minutes=30),
            preGeneratedEncryptionKeys=None,
        )
        response = client.post("master-key-list", key)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # allow without PGEKs
        key = get_key(userId=lecturer.pk)
        key.pop("preGeneratedEncryptionKeys")
        response = client.post("master-key-list", key)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_add_master_key_as_lecturer_with_pgek(self):
        lecturer = self.get_lecturer()
        client = self.get_client(user=lecturer)

        response = client.post("master-key-list", get_key(userId=lecturer.pk))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class KeyReceiveTest(APIEndpointTest):
    def setUp(self) -> None:
        student = self.get_student()
        client = self.get_client(user=student)

        response = client.post(
            "master-key-list",
            get_key(
                userId=student.pk,
                createdAt=datetime.now() - timedelta(minutes=59),
            ),
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        return super().setUp()

    def test_get_empty_as_student(self):
        client = self.get_client(user=self.get_student(username="AnotherStudent"))

        response = client.get("master-key-list")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

    def test_get_as_student(self):
        client = self.get_client(user=self.get_student())

        response = client.get("master-key-list")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_get_as_lecturer(self):
        client = self.get_client(user=self.get_lecturer())
        response = client.get("master-key-list")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        student1 = self.get_student()
        s1_client = self.get_client(user=student1)
        t = datetime.now() - timedelta(minutes=30)
        s1_client.post("master-key-list", get_key(userId=student1.pk, createdAt=t))
        s1_client.post("master-key-list", get_key(userId=student1.pk))

        student2 = self.get_student(username="Student2")
        s2_client = self.get_client(user=student2)
        s2_client.post("master-key-list", get_key(userId=student2.pk))

        for k in UserMasterKey.objects.filter(user=student1).exclude(
            invalidatedAt=None
        ):
            k.createdAt = k.createdAt - timedelta(days=1)
            k.invalidatedAt = k.invalidatedAt - timedelta(days=1)
            k.save()

        # test ?since=DATE
        response = client.get(
            "master-key-list",
            data={"since": date_to_str(datetime.now() - timedelta(minutes=1))},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

        # test ?username=USERNAME
        response = client.get("master-key-list", data={"userid": student1.pk})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

        # test ?onlyValid=True
        response = client.get("master-key-list", data={"onlyValid": "true"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

        # test all together
        response = client.get(
            "master-key-list",
            data={
                "since": date_to_str(datetime.now() - timedelta(minutes=1)),
                "userid": student1.pk,
                "onlyValid": "true",
            },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
