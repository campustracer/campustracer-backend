from rest_framework import status
from api.tests.test_check_in import check_in

from api.tests.utils import APIEndpointTest


class UserAttendedLectureTest(APIEndpointTest):
    def test_get_attendance_as_university(self):
        second_entry_id = "MY_SECOND_HASH_VALUE"
        client = self.get_client()
        response = client.post("check-in-list", check_in())
        response = client.post(
            "check-in-list", check_in(SALHash=second_entry_id, LASHash=second_entry_id)
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        client = self.get_client(self.get_university())
        response = client.get("user-attended-lecture-list")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

        response = client.post(
            "user-attended-lecture-list",
            data=[second_entry_id],
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_get_known_hashes_as_user(self):
        entry_id = "ENTRY_ID"
        client = self.get_client()
        response = client.post(
            "check-in-list", check_in(SALHash=entry_id, LASHash=entry_id)
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        client = self.get_client(user=self.get_student())
        response = client.post("user-attended-lecture-list", [entry_id])
        self.assertContains(response, entry_id)
