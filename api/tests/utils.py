from __future__ import annotations
from typing import Any, Dict, Optional, List, Union

from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIClient

from api import models

METHODS = ["GET", "POST"]


class RestClient(APIClient):
    def reverse(self, path: Union[str, List[str]]) -> str:
        if isinstance(path, str):
            return reverse(path)
        return reverse(path[0], path[1:])

    def get(
        self,
        path: Union[str, List[str]],
        data: Optional[Union[Dict[str, Any], str]] = None,
        follow: bool = False,
        **extra: Any
    ):
        return super().get(
            self.reverse(path),
            data=data,
            follow=follow,
            format=extra.pop("format", "json"),
            **extra
        )

    def post(
        self,
        path: Union[str, List[str]],
        data: Optional[Any] = None,
        format: Optional[str] = None,
        content_type: Optional[str] = None,
        follow: bool = False,
        **extra: Any
    ) -> Response:
        return super().post(
            self.reverse(path),
            data=data,
            format=format if format else "json",
            content_type=content_type,
            follow=follow,
            **extra
        )

    def put(
        self,
        path: Union[str, List[str]],
        data: Optional[Any] = None,
        format: Optional[str] = None,
        content_type: Optional[str] = None,
        follow: bool = False,
        **extra: Any
    ) -> Response:
        return super().put(
            self.reverse(path),
            data=data,
            format=format if format else "json",
            content_type=content_type,
            follow=follow,
            **extra
        )


class APIEndpointTest(APITestCase):
    ENDPOINT = None
    student: models.User | None = None
    lecturer: models.User | None = None
    university: models.User | None = None

    def get_client(self, user: models.User | None = None) -> RestClient:
        """Get an optionally authenticated APIClient

        :param user: The user to authenticate as
        :return: The APIClient
        """
        client = RestClient()
        if user:
            client.force_authenticate(user=user.user)
        return client

    def get_user(self, field: str, role: models.Role, username: str | None = None):
        if username:
            return models.User.create(username=username, role=role)
        if not getattr(self, field):
            setattr(self, field, models.User.create(username="Test" + field, role=role))
        return getattr(self, field)

    def get_student(self, username: str | None = None):
        return self.get_user("student", models.Role.STUDENT, username)

    def get_lecturer(self, username: str | None = None):
        return self.get_user("lecturer", models.Role.LECTURER, username)

    def get_university(self, username: str | None = None):
        return self.get_user("university", models.Role.UNIVERSITY, username)
