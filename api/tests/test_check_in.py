from datetime import datetime

from rest_framework import status

from api.tests.utils import APIEndpointTest
from api.utils import date_to_str


def check_in(
    createdAt: str = date_to_str(datetime.now().date()),
    SALHash: str = "SALHASH" * 6 + "==",
    SAL: str = "EncryptedSALData",
    LASHash: str = "LASHASH" * 6 + "==",
    LAS: str = "EncryptedLASData",
):
    return {
        "createdAt": createdAt,
        "checkInPrivateKeyHash": SALHash,
        "encUserAttendedLecture": SAL,
        "lecturePrivateKeyHash": LASHash,
        "encLectureAttendedByUser": LAS,
    }


class UserAttendedLectureTest(APIEndpointTest):
    def test_create_attendance_as_student(self):
        student = self.get_student()
        client = self.get_client(user=student)
        response = client.post("check-in-list", check_in())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_attendance_as_lecturer(self):
        lecturer = self.get_lecturer()
        client = self.get_client(user=lecturer)
        response = client.post("check-in-list", check_in())
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_attendance(self):
        client = self.get_client()
        response = client.post("check-in-list", check_in())
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
