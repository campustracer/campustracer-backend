from typing import TYPE_CHECKING, Optional

from django.db.models import Q
from django.db.models.query import QuerySet

from rest_framework import mixins, viewsets
from rest_framework.response import Response

from api import models, permissions, serializers
from api.utils import str_to_datetime, str_to_int

if TYPE_CHECKING:
    mixin_base = viewsets.GenericViewSet
else:
    mixin_base = object


class UserMixin(mixin_base):
    _user: Optional[models.User] = None

    @property
    def user(self) -> models.User:
        if not self._user:
            self._user = models.User.objects.get(user=self.request.user)
        return self._user

    def is_lecturer(self) -> bool:
        return self.user.isLecturer

    def is_student(self) -> bool:
        return self.user.isStudent

    def is_university(self) -> bool:
        return self.user.isUniversity


class UserView(
    UserMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):
    """
    Get user information and set encContactData

    Students can access their own entry

    University staff and lecturers can access all entries
    They can receive all changed users since datetime with the parameter since:
        /User/?since=2021-10-20T14:10:53.123
    """

    permission_classes = [permissions.IsAuthenticated]
    serializer_class = serializers.UserSerializer
    lookup_field = "user__username"

    def get_queryset(self):
        queryset = models.User.objects.all().select_related("user")
        if self.is_lecturer() or self.is_university():
            keys_since = str_to_datetime(self.request.query_params.get("since", ""))
            if keys_since:
                return queryset.filter(changedAt__gt=keys_since)
            return queryset
        return queryset.filter(user=self.request.user)


class UserMasterKeyView(
    UserMixin, mixins.CreateModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet
):
    """
    Create and list master and pre generated keys

    Students can create new keys and inspect their currently saved keys.

    Lecturers can create new keys and list all keys.

    University staff and lecturers can access all keys
    They can receive all changed keys since datetime with the parameter since:
        /UserMasterKey/?since=2021-10-20T14:10:53.123
    They can receive all keys belonging to a user with the parameter username:
        /UserMasterKey/?userid=12345
    They can receive only valid keys with the parameter only_valid:
        /UserMasterKey/?onlyValid=true
    All parameters can be combined
    """

    permission_classes = [permissions.IsAuthenticated]
    serializer_class = serializers.UserMasterKeySerializer

    def get_queryset(self):
        queryset = models.UserMasterKey.objects.all()

        if self.is_student():
            # limit to own keys
            queryset = queryset.filter(user__user=self.request.user)

        keys_since = str_to_datetime(self.request.query_params.get("since", ""))
        if keys_since:
            queryset = queryset.filter(
                Q(createdAt__gte=keys_since) | Q(invalidatedAt__gte=keys_since)
            )

        user_id = str_to_int(self.request.query_params.get("userid", ""))
        if user_id:
            queryset = queryset.filter(user_id=user_id)

        keys_valid = self.request.query_params.get("onlyValid", "").lower() == "true"
        if keys_valid:
            queryset = queryset.filter(invalidatedAt=None)

        return queryset


class CheckInView(UserMixin, mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    Create UserAttendedLecture and LectureAttendedByUser records together

    Only unauthenticated users can create these to prevent association between
    these records and users
    """

    permission_classes = [permissions.IsNotAuthenticatedWriteOnly]
    serializer_class = serializers.CheckInSerializer
    queryset = models.UserAttendedLecture.objects.none()


class FilterDateMixin(UserMixin):
    """
    Add filtering to endpoints only accessible by university staff
    """

    def filter_date_params(self, queryset: QuerySet) -> QuerySet:
        records_from = str_to_datetime(self.request.query_params.get("from", ""))
        if records_from:
            queryset = queryset.filter(createdAt__gte=records_from)

        records_to = str_to_datetime(self.request.query_params.get("to", ""))
        if records_to:
            queryset = queryset.filter(createdAt__lte=records_to)

        return queryset


class UserAttendedLectureView(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    FilterDateMixin,
    viewsets.GenericViewSet,
):
    """
    List UserAttendedLecture records

    Only university staff can access these records
    - They can be filtered by ?from=date&to=date parameters
    - IDs can be filtered by posting them in a JSON list to the endpoint
    - Filtering and posting can be combined

    I.e. `POST '["hash1", "hash2"]' /endpoint/?from=dateFrom&to=dateTo`
    will return entries with hashes "hash1" and "hash2" if they where created between
    dateFrom and dateTo
    """

    permission_classes = [
        permissions.IsAuthenticatedWriteOnly | permissions.isUniversity
    ]
    serializer_class = serializers.UserAttendedLectureSerializer
    queryset = models.UserAttendedLecture.objects.all()

    def get_queryset(self) -> QuerySet:
        queryset = super().get_queryset()
        return self.filter_date_params(queryset)

    def create(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter(
            check_in_private_key_hash__in=self.request.data
        )
        # create is used for searching - therefore it should return many
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class LectureAttendedByUserView(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    FilterDateMixin,
    viewsets.GenericViewSet,
):
    """
    List LectureAttendedByUser records

    Only university staff can access these records
    - They can be filtered by ?from=date&to=date parameters
    - IDs can be filtered by posting them in a JSON list to the endpoint
    - Filtering and posting can be combined

    I.e. `POST '["hash1", "hash2"]' /endpoint/?from=dateFrom&to=dateTo`
    will return entries with hashes "hash1" and "hash2" if they where created between
    dateFrom and dateTo
    """

    permission_classes = [permissions.isUniversity]
    serializer_class = serializers.LectureAttendedByUserSerializer
    queryset = models.LectureAttendedByUser.objects.all()

    def get_queryset(self) -> QuerySet:
        queryset = super().get_queryset()
        return self.filter_date_params(queryset)

    def create(self, request, *args, **kwargs):
        # create is used for searching - therefore it should return many
        queryset = self.get_queryset().filter(
            lecture_private_key_hash__in=self.request.data
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class InfectionCase(
    UserMixin,
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):
    """
    Create infection case records

    Only unauthenticated users can create these to prevent association between
    these records and users

    Only university staff can access these records
    Can be filtered by ?sinceId=<>
    """

    permission_classes = [
        permissions.IsNotAuthenticatedWriteOnly | permissions.isUniversityReadOnly
    ]
    queryset = models.InfectionCase.objects.all()
    serializer_class = serializers.InfectionCaseSerializer

    def get_queryset(self):
        if self.is_university():
            queryset = models.InfectionCase.objects.all()

            since_id = str_to_int(self.request.query_params.get("sinceId", ""))
            if since_id:
                queryset = queryset.filter(Q(pk__gt=since_id))

            return queryset
        else:
            return models.InfectionCase.objects.none()
