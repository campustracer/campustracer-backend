def get_log_config(LOG_LEVEL):
    return {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "format": "{levelname} {asctime} {message}",
                "style": "{",
                "datefmt": "%Y-%m-%d %H:%M:%S",
            },
            "verbose": {
                "format": "{levelname} {asctime} {module} {process:d} {thread:d} {message}",  # noqa: E501
                "style": "{",
                "datefmt": "%Y-%m-%d %H:%M:%S",
            },
        },
        "handlers": {
            "console": {
                # Log to stdout
                "class": "logging.StreamHandler",
                "formatter": "simple",
            },
            "null": {
                # No logs
                "level": "DEBUG",
                "class": "logging.NullHandler",
            },
        },
        "loggers": {
            "": {"handlers": ["console"], "level": LOG_LEVEL},
            "django.db.backends": {
                # Disable query logging
                "handlers": ["null"],
                "propagate": False,
                "level": "DEBUG",
            },
        },
        "mozilla_django_oidc": {
            "handlers": ["console"],
            "level": "DEBUG",
        },
    }
