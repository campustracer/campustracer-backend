import os

from config.defaults import *

# Enable debug settings
DEBUG = True
LOG_LEVEL = "DEBUG"

# Allowed hosts, by default only localhost can connect
ALLOWED_HOSTS = ["127.0.0.1", "localhost"]

# About 48 chars of randomness
SECRET_KEY = "just for testing purposes"

# Static files path for deployment
# STATIC_ROOT = "/project/path/static/"

# Database settings
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "HOST": "postgres",
        "PORT": "5432",
        "NAME": os.getenv("POSTGRES_DB"),
        "USER": os.getenv("POSTGRES_USER"),
        "PASSWORD": os.getenv("POSTGRES_PASSWORD"),
    },
}
