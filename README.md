# Getting started

```
# Create and activate venv
python -m venv $VENVPATH
source $VENVPATH/bin/activate

# Install python packages
pip install -r requirements/development.txt

# Create local config
cp config/settings.py.dist config/settings.py
$EDITOR config/local_settings.py.dist

# Setup database and superuser
./manage.py migrate
./manage.py createsuperuser

# Run Django
./manage.py runserver [[addr:]port]
```

